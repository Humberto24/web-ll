const express = require('express');
const cors = require("cors");
const app = express();  
const mongoose = require("mongoose");
const db = mongoose.connect("mongodb://127.0.0.1:27017/tasks-api");
const Task = require("./taskModel");


// check for cors
app.use(cors({
  domains: '*',
  methods: "*"
}));


// listen to GET requests on /hello
app.get('/hello', function (req, res) {
  res.send('Hello World');
});


app.get('/tipocambio', function (req, res) {
  res.send(`{
    "TipoCompraDolares" : "608",
    "TipoVentaDolares" : "621",
    "TipoCompraEuros" : "731.85",
    "TipoVentaEuros" : "761.9"
  }`);
});


app.post('/taskPost', function (req, res) {
  const task = new Task();

  task.title = req.body.title;
  task.description = req.body.description;

  if (task.title && task.description) {
    task.save(function (err) {
      if (err) {
        res.status(422);
        console.log('error al guardar', err)
        res.json({
          error: 'error al guardar'
        });
      }
      res.status(201);//CREATED
      res.header({
        'location': `http://localhost:3000/api/tasks/?id=${task.id}`
      });
      res.json(task);
    });
  } else {
    res.status(422);
    console.log('error al guardar');
    res.json({
      res,
      error: 'Datos invalidos'
    });
  }
});

app.get('/taskGet', function (req, res){
  if (req.query && req.query.id) {
    Task.findById(req.query.id, function (err, task) {
      if (err) {
        res.status(404);
        console.log('error while queryting the task', err)
        res.json({ error: "Task doesnt exist" })
      }
      res.json(task);
    });
  } else {
    Task.find(function (err, tasks) {
      if (err) {
        res.status(422);
        res.json({ "error": err });
      }
      res.json(tasks);
    });
  }
});


app.patch('/taskPatch', function (req, res){
  if (req.query && req.query.id) {
    Task.findById(req.query.id, function (err, task) {
      if (err) {
        res.status(404);
        console.log('error while queryting the task', err)
        res.json({ error: "Task doesnt exist" })
      }

      task.title = req.body.title ? req.body.title : task.title;
      task.detail = req.body.detail ? req.body.detail : task.detail;

      task.save(function (err) {
        if (err) {
          res.status(422);
          console.log('error while saving the task', err)
          res.json({
            error: 'There was an error saving the task'
          });
        }
        res.status(200); 
        res.json(task);
      });
    });
  } else {
    res.status(404);
    res.json({ error: "Task doesnt exist" })
  }
});


  

app.listen(3000, () => console.log(`Example app listening on port 3000!`))
