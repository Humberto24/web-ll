const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const product = new Schema ({
    quantity: {type: String},
    name: {type: String},
    price: {type: String}
});

module.exports = mongoose.model("products", product);