const { buildSchema } = require('graphql');
exports.graphQLschema = buildSchema(`
  type Query {
    orders: [Order]
    clients: [Client]
    getOrder(orderId: Int!): Order
    hello: String
  }

  type Mutation {
    addClient(name: String!, lastName: String! ): Client
    createProduct(quantity: String!, name: String!, price: String!): Product
  }

  type Order {
    client: Client!
    products: [Product!]
  }

  type Client {
    id: ID
    name: String!
    lastName: String!
    email: String
    website: String
  }

  type Product {
    id: String
    quantity: String!
    name: String!
    price: String!
  }`);