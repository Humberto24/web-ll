import React from "react";
import Card from "./Card";


const CardList = ({robots, searchChange}) => {
    console.log("robot",robots)
    //quiere decir que viene en array
    if(robots.length){
        const newRobots = robots.slice(0,10);
        return(
            <div className="tc">
                {
                    newRobots.map((user, i) => {
                        return (
                        <Card
                            key={robots[i].id}
                            id={robots[i].id}
                            name={robots[i].name}
                            state={robots[i].state}
                            country={robots[i].country}
                            searchChange={searchChange}
                        />
                        )
                    } )
                }
            </div>
        )
    }else{
        // var res = Object.values(robots);
        // res.map((element, i) => {
        //     console.log("element",element)
        //     return element
        // }) 
        // console.log("res",res)
        const res = Object.values(robots);
        return (
            res.map((element, i) => {
                if(element !== null){
                    return(
                        element + " ,    "
                    )
                }
            })
        )
    }    
}

export default CardList;