import React from "react";

const Card = ({id, name, state, country, searchChange}) => {
    return(
        <div onClick={searchChange} className="w-30 tc bg-light-blue dib pa3 ma2 grow shadow-5">
            <p>{id}</p>
            <h2>{name}</h2>
            <h2>{state}</h2>
            <h5>{country}</h5>
        </div>
    )
}

export default Card;